from dataclasses import dataclass
from random import randint
import enum

class Rank(enum.IntEnum):
    INIT = -1
    TWO = 0
    THREE = 1
    FOUR = 2
    FIVE = 3
    SIX = 4
    SEVEN = 5
    EIGHT = 6
    NINE = 7
    TEN = 8
    JACK = 9
    QUEEN = 10
    KING = 11
    ACE = 12

class Suit(enum.IntEnum):
    HEART = 0
    DIAMOND = 1
    SPADE = 2
    CLUB = 3

class Result(enum.IntEnum):
    HIGH_CARD = 0
    PAIR = 1
    DOUBLE_PAIR = 2
    THREE_OF_A_KIND = 3
    STRAIGHT = 4
    FLUSH = 5
    FULL = 6
    FOUR_OF_A_KIND = 7
    STRAIGHT_FLUSH = 8
    ROYAL_FLUSH = 9

@dataclass
class Force:
    result: Result
    a: Rank = -1
    b: Rank = -1
    c: Rank = -1
    d: Rank = -1
    e: Rank = -1

@dataclass
class Card:
    rank: Rank
    suit: Suit
    display: list = None

class UserInputError(Exception):
    pass

deck = []

def explain(first, second):
        if (first.result != second.result):
            print("First hand had a {} whereas second hand had a {}".format(first.result.name, second.result.name))
        else:
            if (first.a != second.a):
                print("Both had {} but first hand had {} whereas second had {}".format(first.result.name, first.a.name, second.a.name))
            else:
                if (first.b != second.b):
                    print("Both had {} with {}, but first had {} and second had {}".format(first.result.name, first.a.name, first.b.name, second.b.name))
                else:
                    if (first.c != second.c):
                        print("Both had {} with {} and {}, but first had {} and second had {}".format(first.result.name,\
                                first.a.name, first.b.name, first.c.name, second.c.name))
                    else:
                        if (first.d != second.d):
                            print("Both had {} with {}, {} and {}, but first had {} and second had {}".format(first.result.name, first.a.name, first.b.name,\
                                    first.c.name, first.d.name, second.d.name))
                        else:
                            if (first.e != second.e):
                                print("Both had {} with {}, {}, {} and {}, but first had {} and second had {}".format(first.result.name,\
                                        first.a.name, first.b.name, first.c.name, first.d.name, first.e.name, second.e.name))
                            else:
                                print("Both had {}".format(first.result.name))

def determine_force(straight, flush, f, bmulti, smulti, h, hh, hhh, hhhh, hand):
    if straight != None or flush != None:
        if straight == Rank.ACE and f == 1:
            return Force(Result.Royal_FLUSH, -1, -1, -1, -1, -1)
        elif f == 1:
            return Force(Result.STRAIGHT_FLUSH, straight, -1, -1, -1, -1)
    if bmulti == 4:
        return Force(Result.FOUR_OF_A_KIND, h, hh, -1, -1, -1)
    if bmulti == 3 and smulti == 2:
        return Force(Result.FULL, h, hh, -1, -1, -1)
    if straight != None or flush != None:
        if flush != None:
            return Force(Result.FLUSH, flush[0].rank, flush[1].rank, flush[2].rank, flush[3].rank, flush[4].rank)
        return Force(Result.STRAIGHT, straight, -1, -1, -1, -1)
    if bmulti == 3:
        return Force(Result.THREE_OF_A_KIND, h, hh, hhh, -1, -1)
    if bmulti == 2:
        if smulti == 2:
            return Force(Result.DOUBLE_PAIR, h, hh, hhh, -1, -1)
        return Force(Result.PAIR, h, hh, hhh, hhhh, -1)
    if bmulti == 1:
        return Force(Result.HIGH_CARD, hand[0].rank, hand[1].rank,hand[2].rank, hand[3].rank, hand[4].rank)

def detect_high_a(hand, h, hh, hhh):
    for i in range(7):
        if hand[i].rank == h or hand[i].rank == hh or hand[i].rank == hhh:
            continue
        return hand[i].rank

def detect_high(hand, h, hh):
    for i in range(7):
        if hand[i].rank == h or hand[i].rank == hh:
            continue
        return hand[i].rank

def detect_pair(hand, high, b):
    if b == 2:
       for i in range(6):
            j = i
            k = 0
            if high == hand[i].rank:
                continue
            while k < 2 and j < 7 and hand[i].rank == hand[j].rank:
                k += 1
                j += 1
            if k == 2:
                return 2, hand[i].rank
    for l in range(7):
        if high == hand[l].rank:
            continue
        return 1, hand[l].rank

def detect_multiples(hand):
    for b in range(4, 1, -1):
        for i in range(7 - b + 1):
            j = i
            k = 0
            while k < b and j < 7 and hand[i].rank == hand[j].rank:
                k += 1
                j += 1
            if k == b:
                return b, hand[i].rank
    return 1, hand[0].rank

def detect_straight(hand):
    for i in range(3):
        k = 1
        f = 1
        highest = hand[i].rank
        for j in range(i + 1, 7):
            if hand[i].rank == hand[j].rank + k:
                k += 1
            if hand[i].suit != hand[j].suit:
                f = 0
        if k == 5 or (k == 4 and highest == Rank.FIVE and hand[0].rank == Rank.ACE):
            return highest, f
    return None, 0

def sortRank(card):
    return (int(card.rank))

def detect_flush(hand):
    for i in range(3):
        flush = []
        k = 0
        for j in range(7):
            if hand[i].suit == hand[j].suit:
                k += 1
                flush.append(hand[j])
            if k == 5:
                flush.sort(key = sortRank, reverse = True)
                return flush
    return None

def hand_force(hand):
    bmulti = None
    smulti = None
    h = None
    hh = None
    hhh = None
    hhhh = None
    flush = detect_flush(hand)
    hand.sort(key = sortRank, reverse = True)
    straight, f = detect_straight(hand)
    if flush == straight == None:
        bmulti, h = detect_multiples(hand)
        if bmulti in [2, 3, 4]:
            smulti, hh = detect_pair(hand, h, 1 if bmulti == 4 else 2)
            if bmulti == 3 or bmulti == 2:
                hhh = detect_high(hand, h, hh)
            if bmulti == 2 and smulti == 1:
                hhhh = detect_high_a(hand, h, hh, hhh)
    Force = determine_force(straight, flush, f, bmulti, smulti, h, hh, hhh, hhhh, hand)
    return Force

def analyse_cards():
    first_hand = [deck[0], deck[1], deck[2], deck[3], deck[4], deck[5], deck[6]]
    second_hand = [deck[0], deck[1], deck[2], deck[3], deck[4], deck[7], deck[8]]
    first = hand_force(first_hand)
    second = hand_force(second_hand)
    explain(first, second)
    if first.result == second.result:
        if first.a == second.a:
            if first.b == second.b:
                if first.c == second.c:
                    if first.d == second.d:
                        if first.e == second.e:
                            return ('n')
                        else:
                            return ('l', 'r') [first.e < second.e]
                    else:
                        return ('l', 'r') [first.d < second.d]
                else:
                    return ('l', 'r') [first.c < second.c]
            else:
                return ('l', 'r') [first.b < second.b]
        else:
            return ('l', 'r') [first.a < second.a]
    else:
        return ('l', 'r') [first.result < second.result]

def color(suit):
    if suit in [ suit.DIAMOND, suit.HEART]:
        return("\033[1;31m")
    if suit in [ suit.CLUB, suit.SPADE]:
        return("")

def display_cards():
    for i in range(9):
        print(color(deck[0].suit) + "\t{}".format(deck[0].display[i][0]) + "\033[0m", end = '')
        print(color(deck[1].suit) + "   {}".format(deck[1].display[i][0]) + "\033[0m", end = '')
        print(color(deck[2].suit) + "   {}".format(deck[2].display[i][0]) + "\033[0m", end = '')
        print(color(deck[3].suit) + "   {}".format(deck[3].display[i][0]) + "\033[0m", end = '')
        print(color(deck[4].suit) + "   {}".format(deck[4].display[i][0]) + "\033[0m")
    print("\n")
    for i in range(9):
        print(color(deck[5].suit) + "\t{}".format(deck[5].display[i][0]) + "\033[0m", end = '')
        print(color(deck[6].suit) + "   {}".format(deck[6].display[i][0]) + "\033[0m", end = '')
        print("\t\t\t", end = '')
        print(color(deck[7].suit) + "{}".format(deck[7].display[i][0]) + "\033[0m", end = '')
        print(color(deck[8].suit) + "   {}".format(deck[8].display[i][0]) + "\033[0m")

def shuffle_deck():
     for i in range(52):
         pos = randint(0, 51)
         temp = deck[i]
         deck[i] = deck[pos]
         deck[pos] = temp

def design(rank, suit):
    lines = [[] for i in range(9)]
    name = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
    symbol = ['♥', '♦', '♣', '♠']
    if rank == Rank.TEN:
        space = ''
    else:
        space = ' '
    lines[0].append('┌───────────┐')
    lines[1].append('│ {}{}        │'.format(name[int(rank)], space))
    lines[2].append('│           │')
    lines[3].append('│           │')
    lines[4].append('│     {}     │'.format(symbol[suit]))
    lines[5].append('│           │')
    lines[6].append('│           │')
    lines[7].append('│        {}{} │'.format(space, name[int(rank)]))
    lines[8].append('└───────────┘')
    return lines

# print("\033[{}m{}\033[0m".format(color, carte, carte2))
#
#

def init_deck():
    i = 0
    for r in Rank:
        if r == Rank.INIT:
            continue
        for s in Suit:
            deck.append(Card(r, s, None))
            deck[i].display = design(r, s)
            i += 1

def main():
    init_deck()
    while True:
        shuffle_deck()
        display_cards()
        while True:
            try:
                answer = input("Answer: ")
                if answer in ['l', 'r', 'n', 'quit', 'exit']:
                    break
                raise UserInputError
            except UserInputError:
                print("Please type l for left, r for right, n for none, quit to quit")
        if answer in ['exit', 'quit']:
            exit()
        best_hand = analyse_cards()
        if answer == best_hand:
            print("YES !")
        else:
            print("NO: {}".format(best_hand))
    
if __name__ == "__main__":
    main()
